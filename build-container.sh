#!/usr/bin/env bash

# Builds nc-sdk-docker-builder container
# And then automatically pushes to dockerhub if no error

set -euo pipefail

CONTAINER_NAME="chipbox-recent-toolchain"


docker build -t ${CONTAINER_NAME} .

docker login

docker tag ${CONTAINER_NAME}:latest stulluk/${CONTAINER_NAME}:latest

docker push stulluk/${CONTAINER_NAME}
