# chipbox-recent-toolchain

This is a working recent toolchain to built initrd and kernel for chipbox

Downloaded from ARM Website in this link:

https://developer.arm.com/-/media/Files/downloads/gnu-a/8.2-2019.01/gcc-arm-8.2-2019.01-x86_64-arm-linux-gnueabi.tar.xz

## Usage 

- Clone this repository
```sh
git clone git@gitlab.com:stulluk/chipbox-recent-toolchain.git
```
- Decompress to whereever you want on your system
```sh
mkdir -p $HOME/chipbox-toolchain && \
tar xvf gcc-arm-8.2-2019.01-x86_64-arm-linux-gnueabi.tar.xz \
-C $HOME/chipbox-toolchain --strip-components=1
```
- Add it to your $PATH environment variable
```sh
export PATH=$PATH:$HOME/chipbox-toolchain/bin
```

## Test

Run

```sh
$  arm-linux-gnueabi-gcc -v
Using built-in specs.
COLLECT_GCC=arm-linux-gnueabi-gcc
COLLECT_LTO_WRAPPER=/home/stulluk/toolchain/bin/../libexec/gcc/arm-linux-gnueabi/8.2.1/lto-wrapper
Target: arm-linux-gnueabi
Configured with: /tmp/dgboter/bbs/rhev-vm2--rhe6x86_64/buildbot/rhe6x86_64--arm-linux-gnueabi/build/src/gcc/configure --target=arm-linux-gnueabi --prefix= --with-sysroot=/arm-linux-gnueabi/libc --with-build-sysroot=/tmp/dgboter/bbs/rhev-vm2--rhe6x86_64/buildbot/rhe6x86_64--arm-linux-gnueabi/build/build-arm-linux-gnueabi/install//arm-linux-gnueabi/libc --with-bugurl=https://bugs.linaro.org/ --enable-gnu-indirect-function --enable-shared --disable-libssp --disable-libmudflap --enable-checking=release --enable-languages=c,c++,fortran --with-gmp=/tmp/dgboter/bbs/rhev-vm2--rhe6x86_64/buildbot/rhe6x86_64--arm-linux-gnueabi/build/build-arm-linux-gnueabi/host-tools --with-mpfr=/tmp/dgboter/bbs/rhev-vm2--rhe6x86_64/buildbot/rhe6x86_64--arm-linux-gnueabi/build/build-arm-linux-gnueabi/host-tools --with-mpc=/tmp/dgboter/bbs/rhev-vm2--rhe6x86_64/buildbot/rhe6x86_64--arm-linux-gnueabi/build/build-arm-linux-gnueabi/host-tools --with-isl=/tmp/dgboter/bbs/rhev-vm2--rhe6x86_64/buildbot/rhe6x86_64--arm-linux-gnueabi/build/build-arm-linux-gnueabi/host-tools --with-arch=armv7-a --with-pkgversion='GNU Toolchain for the A-profile Architecture 8.2-2019.01 (arm-rel-8.28)'
Thread model: posix
gcc version 8.2.1 20180802 (GNU Toolchain for the A-profile Architecture 8.2-2019.01 (arm-rel-8.28)) 
$
```


