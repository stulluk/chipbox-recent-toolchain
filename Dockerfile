FROM ubuntu:20.04

COPY gcc-arm-8.2-2019.01-x86_64-arm-linux-gnueabi.tar.xz /opt/crosstool/



RUN apt update && apt -y upgrade

ARG DEBIAN_FRONTEND=noninteractive

#RUN apt install -y build-essential cmake gcc-multilib g++-multilib silversearcher-ag vim libtool pkg-config cmake libncurses-dev xz-utils

RUN apt install -y xz-utils


RUN cd /opt/crosstool && tar xvf * --strip-components=1
ENV PATH "$PATH:/opt/crosstool/bin/"


